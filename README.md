Ukázkový web nad frameworkem Hexo
=================================

Vytvoření webu
--------------

```shell
npm install -g hexo-cli
npm install
hexo server
```

Pokud je na stejném počítači více uživatelů, spusťtě
s parametrem `-p`:

```shell
hexo server -p 4001
```

