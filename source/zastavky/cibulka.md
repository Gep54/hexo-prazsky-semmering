---
title: Cibulka
layout: zastavka
ordering: 30
details:
  gps:	50°3′52,53″ s. š., 14°21′43,75″ v. d.
  request: true
  tracks: 1
  altitude: 320
  picture: jinonice.jpg
  picture_cc: 'VitVit, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en">CC BY-SA 4.0</a>'
  picture_url: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Praha-Cibulka_ozna%C4%8Den%C3%AD_zast%C3%A1vky.jpg/220px-Praha-Cibulka_ozna%C4%8Den%C3%AD_zast%C3%A1vky.jpg"
---

>Lorem ipsum dolor sit amet consectetuer accumsan vel mollis tincidunt lobortis.
>Id id vitae vitae Maecenas Vivamus Sed eget euismod natoque et.
>Integer purus magna vel Integer nunc sagittis orci quis tellus enim.
>Duis at et ac Vestibulum hendrerit ac nibh sed nec amet.
>Dis adipiscing Aenean aliquet dolor Curabitur Sed senectus et neque natoque.
>
>Velit quis wisi elit ac Donec Curabitur quis urna Cras tellus. 
>
>-[Wikipedia](https://cs.wikipedia.org/wiki/Praha-Cibulka_(%C5%BEelezni%C4%8Dn%C3%AD_zast%C3%A1vka)
